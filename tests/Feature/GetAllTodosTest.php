<?php

namespace Tests\Feature;

use App\Models\Todo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetAllTodosTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    private $todos;

    public function setUp(): void
    {
        parent::setUp();
        $this->todos = Todo::factory(3)->create();
    }


    public function test_get_all_todos_in_response()
    {

        $resp = $this->json('GET', route('todo.index'));

        $resp->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
            $json->has(3)
                ->first(fn ($json) =>
                $json->where('text', 'buy some milk')
                    ->etc()
                )
            );

    }
}
