<?php

use App\Http\Controllers\TodoController;
use Illuminate\Support\Facades\Route;

Route::get('todos', [TodoController::class, 'index'])->name('todo.index');
Route::post('store-todo', [TodoController::class, 'store'])->name('todo.add');
