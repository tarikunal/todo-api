<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TodoController extends Controller
{
    public function index(Request $request)
    {
        return Todo::latest()->get();
    }

    public function store(Request $request)
    {
        $todo = new Todo();
        $todo->text = $request->get('text');
        $todo->save();
        
        return response()->json([
            'message' => 'Todo successfully created.'
        ]);
    }


}
