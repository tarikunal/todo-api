<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateTodoTest extends TestCase
{
    use RefreshDatabase, WithFaker;


    public function test_it_creates_a_new_todo()
    {
        $this->json('POST', route('todo.add'), [
            'text' => 'buy some milk',
        ]);

        $this->assertDatabaseHas('todos', [
            'id' => 1,
            'text' => 'buy some milk'
        ]);
    }

    public function test_it_returns_success_message_in_response()
    {

        $resp = $this->json('POST', route('todo.add'), [
            'text' => 'buy some milk',
        ]);

        $resp->assertStatus(200)
            ->assertJson([
                'message' => 'Todo successfully created.'
            ]);

    }
}
